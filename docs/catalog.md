# Katalog

--- 

## Info Relaksasi

Service ini digunakan untuk mengetahui informasi tagihan dan simulasi tagihan relaksasi peserta maupun badan usaha.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/relaksasi/info/(noEntitas)/(jmlBlnMenunggak)"
```

```
noEntitas : nomor kartu atau kode badan usaha
jmlBlnMenunggak : numeric
```

`success response`
```json
{
  "metaData": {
    "code": "200",
    "message": "Peserta berhak mendapatkan relaksasi"
  },
  "response": {
    "noEntitas": "0002085184269",
    "namaEntitas": "DEWI NAUFAL IZZATI",
    "jmlBulanMenunggak": "7",
    "jmlKeluarga": "4",
    "tagSdBulanIni": "1377000",
    "tagBulanBerjalan": "0",
    "tagRangeBulan": "1224000",
    "tagihanRelaksasi": "1224000",
    "sisaTagihan": "153000",
    "detail": [
      {
        "noEntitas": "0001502277941",
        "namaEntitas": "H. ABD. FATAH",
        "jmlBulanMenunggak": "7",
        "statusBisaRelaksasi": "Relaksasi",
        "tagSdBulanIni": "306000",
        "tagBulanBerjalan": "0",
        "tagRangeBulan": "306000",
        "tagihanRelaksasi": "306000",
        "sisaTagihan": "0"
      },
      {
        "noEntitas": "0002085183955",
        "namaEntitas": "WARNI",
        "jmlBulanMenunggak": "7",
        "statusBisaRelaksasi": "Relaksasi",
        "tagSdBulanIni": "357000",
        "tagBulanBerjalan": "0",
        "tagRangeBulan": "306000",
        "tagihanRelaksasi": "306000",
        "sisaTagihan": "51000"
      },
      {
        "noEntitas": "0002085184146",
        "namaEntitas": "MUHAMAD ZIA ULHAK",
        "jmlBulanMenunggak": "7",
        "statusBisaRelaksasi": "Relaksasi",
        "tagSdBulanIni": "357000",
        "tagBulanBerjalan": "0",
        "tagRangeBulan": "306000",
        "tagihanRelaksasi": "306000",
        "sisaTagihan": "51000"
      },
      {
        "noEntitas": "0002085184269",
        "namaEntitas": "DEWI NAUFAL IZZATI",
        "jmlBulanMenunggak": "7",
        "statusBisaRelaksasi": "Relaksasi",
        "tagSdBulanIni": "357000",
        "tagBulanBerjalan": "0",
        "tagRangeBulan": "306000",
        "tagihanRelaksasi": "306000",
        "sisaTagihan": "51000"
      }
    ]
  }
}
```

`failed response`
```json
{
  "metaData": {
    "code": "201",
    "message": "Badan Usaha tidak berhak mengikuti Program Keringanan Pembayaran Tunggakan JKN"
  },
  "response": null
}
```

---

## Simpan Relaksasi

Service ini digunakan untuk menyimpan relaksasi peserta maupun badan usaha

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : post
url : "{BASE_URL}/relaksasi/simpan"
```

```
{
 noEntitas : nomor kartu atau kode badan usaha
 jmlBulanMenunggak : bulan menunggak yang akan di bayar awal saat relaksasi >= 6 
 kdApp : kdapp pendaftar sesuai kdapp di registrasi
 email : alamat email
 noHP : nomor hp
 isCicil : 0 (Lunas) 1 (Cicil)
 tglLunas : format 'yyyy-MM-dd' (Jika isCicil adalah 0 maka diisi; Jika isCicil adalah 1 maka diisi null)
 bln202006 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202007 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202008 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202009 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202010 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202011 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202012 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202101 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202102 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202103 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202104 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202105 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202106 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202107 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202108 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202109 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202110 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202111 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
 bln202112 : 0 (digunakan jika isCicil adalah 1; diisi sesuai dengan jumlah berapa bulan yang akan dibayar pada bulan tersebut)
}
```

`body`
```json
{
 "noEntitas":"0001260827471",
 "jmlBulanMenunggak" : "9",
 "kdApp": "091",
 "email": "xxxx@gmail.com",
 "noHP": "08123456718",
 "isCicil": "0",
 "tglLunas": "2020-05-01",
 "bln202006":"0",
 "bln202007":"0",
 "bln202008":"0",
 "bln202009":"0",
 "bln202010":"0",
 "bln202011":"0",
 "bln202012":"0",
 "bln202101":"0",
 "bln202102":"0",
 "bln202103":"0",
 "bln202104":"0",
 "bln202105":"0",
 "bln202106":"0",
 "bln202107":"0",
 "bln202108":"0",
 "bln202109":"0",
 "bln202110":"0",
 "bln202111":"0",
 "bln202112":"0"
}
```

`success response`
```json
{
  "metaData": {
    "code": "200",
    "message": "Sukses"
  },
  "response": {
    "IDRelaksasi": "4161FF56278DD638C4B9",
    "noEntitas": "0001260827471",
    "isCicil": "0",
    "statusCicil": "LUNAS DI DESEMBER 2021",
    "fDate": "2020-05-27"
  }
}
```

`failed response`
```json
{
  "metaData": {
    "code": "201",
    "message": "Gagal Simpan, pastikan noEntitas benar atau cek history pendaftaran"
  },
  "response": null
}
```