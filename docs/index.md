# Web Service PerPres 64

***

## Latar Belakang

* Menindaklanjuti terbitnya `Peraturan Presiden Nomor 64 Tahun 2020` tentang Perubahan Kedua Perpres 82 Tahun 2018 dimana memutuskan tentang adanya **Program Relaksasi Iuran bagi Peserta PBPU dan PPU**
* Penyampaian `Bussiness Requirement Kedeputian Bidang MIUR No. 496/MIUR/0520` tanggal 20 Mei 2020 hal Usulan **Bussiness Requirement Program Relaksasi Iuran**
* `Hasil Vicon pada tanggal  22 Mei  2020` yang terkait dengan pembahasan Ruang Lingkup Pengembangan Sistem Program Relaksasi, yang dihadiri oleh **Kedeputian Bidang Manajemen Iuran, Kepesertan, OTI, MSR, Perluasan Kepesertaan dan Yanser**
* Penyediaan `tools konfirmasi/pendaftaran dan cek informasi bagi peserta/pemberi kerja` yang akan mengikuti program relaksasi
* Penyesuaian **tagihan iuran pada peserta PBPU dan PPU yang menunggak**
* Terlaksananya `percepatan dan implementasi Perpres No. 64 Tahun 2020` sesuai dengan masa berlaku Perpres dimaksud
* Penyesuaian **skema pembayaran iuran paling banyak 6 bulan** untuk aktif kembali
* Penyediaan **model cicilan untuk peserta PBPU** melunasi sisa tunggakan iuran
* Program Relaksasi berlaku bagi Peserta atau Pemberi Kerja yang mempunyai **tunggakan lebih dari 6 bulan menunggak**

***

## Pedoman dan Regulasi

Perubahan dan penyesuaian sistem Program Relaksasi berdasarkan tindaklanjut `Peraturan Presiden No. 64 Tahun 2020 Pasal 42 ayat 3a.` Untuk mendapatkan **informasi lebih detail terkait dengan Peraturan Presiden dimaksud dan regulasi lain yang terkait dengan Program Relaksasi**, maka dapat dilihat pada [regulasi](http://regulasi.bpjs-kesehatan.go.id)

***

## Bisnis Proses

Adanya perubahan **Bisnis Proses Sistem Aplikasi yang terkait dengan penyesuaian Program Relaksasi.** Dan untuk dapat menggambarkan lebih detail terkait dengan Proses Bisnis Program Relaksasi, maka dapat dilihat melalui [proses bisnis](http://bispro.bpjs-kesehatan.go.id/bispro/)

***

## Flowchart

![Flowchart Program Relaksasi](https://i.ibb.co/VSfGTzR/flowchart-1.png "Flowchart Relaksasi")

* Peserta atau Pemberi Kerja yang berhak mendapatkan program relaksasi adalah **peserta PBPU semua kelas dan PPU BU yang memilki tunggakan lebih dari 6 bulan**
* Pada  Proses  Pendaftaran  hanya  dapat  dilakukan  pada  **tanggal  1-25  bulan berjalan**.
* Peserta atau Pemberi Kerja akan mengajukan program relaksasi dengan jumlah tunggakan yang akan dibayarkan serta kesanggupan pembayaran sisa tunggakan yang harus dibayarkan **sampai dengan Desember 2021 baik dengan pembayaran sekaligus atau dengan skema cicilan**. 
* Peserta melakukan pembayaran sebesar **Total tagihan yang direlaksasikan + Tagihan Iuran bulan berjalan.** Setelah itu status kepesertaan peserta tersebut akan aktif
* Jika dalam masa **periode 45 hari peserta ingin mendapatkan RITL**, maka peserta tersebut tetap **dikenai dengan denda layanan sebesar yang telah ditetapkan.**



